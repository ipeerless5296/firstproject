﻿using AppModel.Mst;
using Infrastructure.Repositories;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Services
{
    public class UserService
    {
        private readonly UserRepository _repository;
        private readonly static ILogger _logger = NLog.LogManager.GetCurrentClassLogger();

        public UserService()
        {
            _repository = new UserRepository();
        }

        public UserService(SqlConnection connection)
        {
            _repository = new UserRepository(connection);
        }

        public UserService(string connectionString)
        {
            _repository = new UserRepository(connectionString);
        }

        public IEnumerable<User> GetAll()
        {
            try
            {
                _logger.Debug("ユーザー情報全取得 [開始]");
                return _repository.GetAll();
            }
            catch (Exception e)
            {
                _logger.Error(e);
                throw;
            }
            finally
            {
                _logger.Debug("ユーザー情報全取得 [終了]");
            }
        }
    }
}
