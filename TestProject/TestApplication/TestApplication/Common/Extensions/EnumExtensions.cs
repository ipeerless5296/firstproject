﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestApplication.Common.Extensions
{
    public static class EnumExtensions
    {
        public static string GetProperName<T>(this T Value) where T : struct, IComparable, IConvertible, IFormattable
        {
            if (!(typeof(T).IsEnum))
            {
                throw new InvalidEnumArgumentException();
            }

            FieldInfo fieldInfo = Value.GetType().GetField(Value.ToString());
            if (fieldInfo == null) return null;

            var attr = (DescriptionAttribute)fieldInfo.GetCustomAttribute(typeof(DescriptionAttribute));
            if (attr == null) return "";
            return attr.Description;
        }
    }
}
