﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    internal class BaseRepository 
    {
        protected readonly string _connectionString = Properties.Settings.Default.ConnectionString;
        protected readonly SqlConnection _connection;

        public BaseRepository() { }

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public BaseRepository(SqlConnection connection)
        {
            _connection = connection;
        }
        protected SqlConnection CreateSqlConnection()
        {
            SqlConnection connection;
            if (_connection != null)
                connection = _connection;
            else
                connection = new SqlConnection(_connectionString);
            connection.Open();
            return connection;
        }
    }
}
