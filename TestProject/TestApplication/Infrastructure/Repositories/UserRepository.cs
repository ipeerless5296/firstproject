﻿using AppModel.Enums;
using AppModel.Mst;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    internal class UserRepository : BaseRepository
    {
        #region コンストラクタ
        public UserRepository()
            : base()
        { }

        public UserRepository(SqlConnection connection)
            : base(connection)
        { }

        public UserRepository(string connectionString)
            : base(connectionString)
        { }

        #endregion

        public IEnumerable<User> GetAll()
        {
            using (var connection = base.CreateSqlConnection())
            using (var command = new SqlCommand() { Connection = connection })
            {
                command.CommandText = @"Select * 
                    from [TestDataBase].[mst].[Users];";
                using (var reader = command.ExecuteReader())
                {
                    var users = new List<User>();
                    while (reader.Read())
                    {
                        var user = new User()
                        {
                            Code = (string)reader["Code"],
                            Name = (string)reader["Name"],
                            Deleted = (bool)reader["Deleted"],
                            AuthoriyLevel = UserAuthTypeProcessor.GetUserAuthType((string)reader["AuthorityLevel"])
                        };
                        users.Add(user);
                    }
                    return users;
                }
            }
        }
    }
}
