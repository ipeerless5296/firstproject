﻿using AppModel.Mst;
using Infrastructure.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestApplication.Forms
{
    public partial class MainForm : Form
    {
        private UserService _userService = new UserService(Properties.Settings.Default.ConnectionString);
        public MainForm()
        {
            InitializeComponent();
            this.OkButton.Click += (_, __) => ExecOk();
            this.UserDataGrid.DataSource = new User[0]; 
        }

        private void ExecOk()
        {
            this.UserDataGrid.DataSource = _userService.GetAll().ToArray();
        }
    }
}
