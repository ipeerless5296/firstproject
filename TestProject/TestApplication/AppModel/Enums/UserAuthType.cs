﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AppModel.Enums
{
    public enum UserAuthType
    {
        [Description("管理者")]
        Master,

        [Description("一般")]
        Normal,

        [Description("ゲスト")]
        Guest,
    }

    public static class UserAuthTypeProcessor
    {
        public static UserAuthType GetUserAuthType(string value)
        {
            switch (value)
            {
                case "M": return UserAuthType.Master;
                case "N": return UserAuthType.Normal;
                case "G":
                default: return UserAuthType.Guest;
            }
        }

        public static string GetDescription(this UserAuthType value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            var desciptionString = attributes.Select(n => n.Description)
                              .FirstOrDefault();
            if (desciptionString != null)
            {
                return desciptionString;
            }
            return value.ToString();
        }
    }
}
