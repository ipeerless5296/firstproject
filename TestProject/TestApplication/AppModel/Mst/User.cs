﻿using AppModel.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppModel.Mst
{
    public class User
    {
        /// <summary>
        /// ユーザーコード
        /// </summary>
        [DisplayName("ユーザーコード")]
        public string Code { get; set; }

        /// <summary>
        /// ユーザー名
        /// </summary>
        [DisplayName("ユーザー名")]
        public string Name { get; set; }

        /// <summary>
        /// 削除フラグ
        /// </summary>
        [DisplayName("削除フラグ")]
        public bool Deleted { get; set; }

        /// <summary>
        /// 権限レベル
        /// </summary>
        [Browsable(false)]
        public UserAuthType AuthoriyLevel { get; set; }

        [Browsable(true)]
        [DisplayName("権限レベル")]
        public string AuthoriyLevelString
        {
            get { return this.AuthoriyLevel.GetDescription(); }
        }

    }
}
